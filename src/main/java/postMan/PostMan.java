package postMan;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class PostMan {

	@Test
	public void launchPostMan() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setBinary("C:\\Users\\Gayatri\\AppData\\Local\\Postman\\app-6.6.1\\postman.exe");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(5000);
		/*WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[contains(text(),'New')]")));*/
		new WebDriverWait(driver, 30).until(ExpectedConditions.numberOfWindowsToBe(4));
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allWindows = new ArrayList<String>(windowHandles);
		System.out.println(windowHandles);  
		driver.switchTo().window(allWindows.get(1)); 
		Thread.sleep(5000);
		driver.findElementByXPath("//div[contains(text(),'New')]").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[text()='Request']"))).click();
		driver.findElementByXPath("//input[contains(@class,'request-name')]").sendKeys("CreateTest");
		driver.findElementByXPath("//div[text()='Request description (Optional)']/following::textarea").sendKeys("Creating a new test request");
		driver.findElementByXPath("//div[@class='explorer__header__create-wrapper']/div[2]").click();
		driver.findElementByXPath("//div[@class='input-search-group explorer-row__create']//input").sendKeys("NewProject");
		driver.findElementByXPath("//div[@class='explorer-row__save']/i").click();
		driver.findElementByXPath("//div[@class='explorer-row__name']").click();
		driver.findElementByXPath("//div[@class='btn btn-primary']/span").click();
	}

}






