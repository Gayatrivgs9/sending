package postMan;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class CreateCollection {

	@Test
	public void createCollection() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setBinary("C:\\Users\\Gayatri\\AppData\\Local\\Postman\\app-6.6.1\\postman.exe");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(7000);
		/*WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[contains(text(),'New')]")));*/
		new WebDriverWait(driver, 30).until(ExpectedConditions.numberOfWindowsToBe(4));
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allWindows = new ArrayList<String>(windowHandles);
		System.out.println(windowHandles);  
		driver.switchTo().window(allWindows.get(1)); 
		Thread.sleep(2000); 
		driver.findElementByXPath("//div[contains(text(),'New')]").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[text()='Collection']"))).click();
        driver.findElementByXPath("//input[contains(@placeholder,'Collection Name')]").sendKeys("CreateCollection");
        driver.findElementByXPath("//div[@class='tab tab-primary tab--authorization']/span").click();
        driver.findElementByXPath("//span[text()='No Auth']").click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath("//span[text()='Basic Auth']"))).click();

	}

}















