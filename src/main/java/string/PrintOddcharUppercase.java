package string;

import org.testng.annotations.Test;

public class PrintOddcharUppercase {
	//Print Odd character in Upper Case.
	String  input = "Gopinath";		
	String  output = "";
	@Test
	public void oddCharUpperCase() {
		char[] len = input.toCharArray();
		for (int i = 0; i < len.length; i++) {		
			if(i%2!=0) {
				output += Character.toUpperCase(input.toCharArray()[i]);		
			} else {
				output += Character.toLowerCase(input.toCharArray()[i]);	
			}		
		}	
		System.out.println(output);
	}
}
