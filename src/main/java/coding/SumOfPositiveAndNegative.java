package coding;

public class SumOfPositiveAndNegative {

	public static void main(String[] args) {
		int[] num = {1,3,5,-6,-1,-5,7};

		int sum = 0;
		for (int i : num) {
			sum = sum+i;
		}
		System.out.println(sum);
	}

}
