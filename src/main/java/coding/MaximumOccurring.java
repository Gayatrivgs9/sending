package coding;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MaximumOccurring {

	public static void main(String[] args) {
		String data = "Welcome To Automation Testing";
		char[] ch = data.toCharArray();
		Map<Character, Integer> map = new LinkedHashMap<>();
		int count =0;
		for (char c : ch) {
			if (map.containsKey(c)) {
				count = map.get(c)+1;
				map.put(c, map.get(c)+1);
			} else {
                map.put(c, 1);
			}
		}
		for (Entry<Character, Integer> each : map.entrySet()) {
			System.out.println(each.getKey()+" --> "+each.getValue());
		}
		
	}

}





