package coding;

import java.util.LinkedHashSet;
import java.util.Set;

public class PrintDuplicates {

	public static void main(String[] args) {
		String data = "Gayatrii";
		char[] ch = data.toCharArray();
		Set<Character> set = new LinkedHashSet<>();
		for (int i = 0; i < ch.length; i++) {
			for (int j = i+1; j < ch.length; j++) {
				if (ch[i] == ch[j]) {
					set.add(ch[j]);
				}
			}
		}
        System.out.println(set);
	}

}
