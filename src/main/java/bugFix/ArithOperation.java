package bugFix;
import java.util.Scanner;

public class ArithOperation {

	public static void main(String[] args) {
		System.out.println("Enter first number");
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		System.out.println("Enter second number");
		int num2 = sc.nextInt();
		System.out.println("Enter the Arithmeic Operator");
		String str = sc.next();
		
		int result;
		if (str.equals("+")) {
			result = num1+num2;
			System.out.println("Addition operation performed " +result);
		}
		else if (str.equals("-")) {
			result = num1-num2;
			System.out.println("Subtraction Operation performed " +result);
		}
		else if (str.equals("*")) {
			result = num1*num2;
			System.out.println("Multiplication operation performed " +result);
		}
		else if (str.equals("/")) {
			result = num1/num2;
			System.out.println("Division operation performed " +result);
		}
		else {
			System.out.println(str+ " is an invalid entry");
		}
		sc.close();
	}

}


/*Output:
-------

Enter the number
5
2
Enter the Arithmeic Operator
+
+ is an invalid entry*/