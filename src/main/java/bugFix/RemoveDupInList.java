package bugFix;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RemoveDupInList {

	public static void main(String[] args) {
		
		List<Integer> ls = new ArrayList<>();
		ls.add(11);
		ls.add(11);
		ls.add(12);
		ls.add(11);
		ls.add(11);
		ls.add(11);
		ls.add(12);
		ls.add(13);
		ls.add(12);
		System.out.println("Duplicate list: "+ls);
		Set<Integer> set = new HashSet<>();
		Object[] array = ls.toArray();
		for (Object num : array) {
			if (ls.indexOf(num)==ls.lastIndexOf(num)) {
				ls.remove(ls.lastIndexOf(num));
			}
		}
		set.addAll(ls);
       System.out.println("Without Dup: "+set);
	}

}
