package bugFix;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Data {

	
	public static Object[][] data() throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/tagNames.xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		short columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("rowCount: "+rowCount+" columnCount: "+columnCount);
		Object[][] data = new Object[rowCount][columnCount];
		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				data[i-1][j]= row.getCell(j).getStringCellValue();
				//System.out.println(stringCellValue);
			}
		}
		wbook.close();
		return data;
	}
}
