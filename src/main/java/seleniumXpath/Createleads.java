package seleniumXpath;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Createleads {


	public static void main(String args[]) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// driver object is created
		WebDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");

		driver.manage().window().maximize();

		//Enter user name
		driver.findElement(By.id("username")).sendKeys("DemoSalesManager");

		//Enter password
		driver.findElement(By.id("password")).sendKeys("crmsfa");

		// Click Login
		driver.findElement(By.className("decorativeSubmit")).click();

		// Click Crmsfa link
		driver.findElement(By.linkText("CRM/SFA")).click();

		// click on create lead
		driver.findElement(By.linkText("Create Lead")).click();

		// enter company name
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys("Alight Solutions");

		//enter firstname
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys("kalai");

		//enter lastname
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys("rajan");

		// selecting value in dropdown
		// to get the return type press CTRL+2L
		WebElement source = driver.findElement(By.id("createLeadForm_dataSourceId"));
		Select select = new Select(source);
		select.selectByVisibleText("Public Relations");

		//market selecting by size 
		WebElement marketvalue = driver.findElement(By.id("createLeadForm_marketingCampaignId"));
		Select marketcam = new Select(marketvalue);

		// storing dropdown values in a list to get the desired option by size
		List<WebElement> options = marketcam.getOptions();
		int size = options.size();
		marketcam.selectByIndex(size-3);
		// to print options in dropdown
		for (WebElement eachOption : options) {
			System.out.println(eachOption.getText());
		}

		// select industry value
		WebElement industry = driver.findElement(By.id("createLeadForm_industryEnumId"));
		Select industryvalue = new Select(industry);
		industryvalue.selectByValue("IND_TELECOM");

		// to print number of dropdowns in a page
		int size2 = driver.findElements(By.xpath("(//table[@cellspacing='0'])[17]//select")).size();
		System.out.println(size2);

		// to return number of rows in DOB field
		List<WebElement> findElements = driver.findElements(By.xpath("(//img[@id='createLeadForm_birthDate-button"));
		int size3 = findElements.size();
		System.out.println(size3);
		
		driver.findElement(By.xpath("//input[@class ='smallSubmit']")).click();
		driver.close();
	}

}
