package seleniumXpath;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class LearningFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();		
		driver.manage().window().maximize();
		//driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().to("https://erail.in/");
		// clears the value in From textbox
		driver.findElement(By.id("txtStationFrom")).clear();
		// pass the value to from textbox
		driver.findElement(By.id("txtStationFrom")).sendKeys("MS",Keys.TAB);
		// clears the value in To textbox
		driver.findElement(By.id("txtStationTo")).clear();
		// pass the value in To textbox
		driver.findElement(By.id("txtStationTo")).sendKeys("SXT",Keys.TAB);
		driver.findElement(By.id("chkSelectDateOnly")).click();
		// list of search found
		int size = driver.findElements(By.xpath("//table[@class='DataTable TrainList']//tr[1]/td")).size();
		System.out.println(size);
		// to choose the particular table value
		List<WebElement> allColumn = driver.findElements(By.xpath("//table[@class='DataTable TrainList']//tr[1]/td"));
		allColumn.get(0).click();
		driver.close();
	}
}
