package doc;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class SentImage {

	public static void main(String[] args) throws InterruptedException {

		
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://www.google.com/");
			File src = driver.getScreenshotAs(OutputType.FILE);
			File des = new File("E:\\Workspace\\SendEmail\\snaps\\img.jpg");
			FileUtils.copyFile(src, des);
			XWPFDocument docx = new XWPFDocument();
			XWPFParagraph par = docx.createParagraph();
			XWPFRun run = par.createRun();
			/*run.setText("Hello Doc");
			run.setFontSize(13);*/
			System.out.println("text written");
			InputStream img;
        	img = new FileInputStream(des);
			try {
				Thread.sleep(2000);
				run.addPicture(img, Document.PICTURE_TYPE_JPEG, "img", Units.toEMU(530), Units.toEMU(250));
				System.out.println("image added");
			} catch (InvalidFormatException e) {
				System.err.println("File Format is wrong");
			}
        FileOutputStream fos = new FileOutputStream("E:\\Workspace\\SendEmail\\document\\a.doc"); 
        docx.write(fos);
        fos.flush();
        img.close();
        fos.close(); 
        docx.close();
		} catch (IOException e) {
			System.err.println("IOException occured");
		}
		System.out.println("success");

	}

}
