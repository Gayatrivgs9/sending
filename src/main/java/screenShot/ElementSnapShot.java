package screenShot;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.RotatingDecorator;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import ru.yandex.qatools.ashot.shooting.ShootingStrategy;
import ru.yandex.qatools.ashot.shooting.ViewportPastingDecorator;
import ru.yandex.qatools.ashot.shooting.cutter.CutStrategy;
import ru.yandex.qatools.ashot.shooting.cutter.VariableCutStrategy;

public class ElementSnapShot {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver1.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().fullscreen();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("password"); 
		AShot aShot = new AShot();
		Screenshot src = aShot.shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
		System.out.println(src);
		/*File des = new File("./snaps/img.png");
		FileUtils.copyFile(srcFile, destFile);*/
		
		
		
		
		
		
		
		/*CutStrategy cutting = new VariableCutStrategy(100, 300, 200);
		ShootingStrategy rotating = new RotatingDecorator(cutting, ShootingStrategies.simple());
		ShootingStrategy pasting = new ViewportPastingDecorator(rotating)
				.withScrollTimeout(5);   
		new AShot()
		.shootingStrategy(pasting)
		.takeScreenshot(driver);*/






		/*File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img.png");
		FileUtils.copyFile(src, dec); */

	}

}
