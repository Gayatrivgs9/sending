package reports;

import java.util.List;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.model.Author;
import com.aventstack.extentreports.model.Test;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.ExtentXReporter;

public class Reports {

	@org.testng.annotations.Test
	public void startResult() {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		// initialize ExtentXReporter("mongodb-host", mongodb-port);
		ExtentXReporter extentx = new ExtentXReporter("localhost", 27017);
		//appending
		extentx.setAppendExisting(true);
		//ExtentX server address 
		extentx.config().setServerUrl("http://localhost:1337");
		ExtentReports extent = new ExtentReports();
		//then add extentx and html
		extent.attachReporter(extentx,html); 
		List<Test> test = extentx.getTestList();
		for (Test t : test) {
			extentx.onTestStarted(t);
			extentx.onNodeStarted(t);  
		}
		// if you have multiple projects set project name
		extentx.config().setProjectName("LeafTaps");  
		// report or build name
		extentx.config().setReportName("SampleReports");
		//to get reportId
		extentx.getReportId();
		//to get ProjectId
		extentx.getProjectId();
		
		
	}
}










